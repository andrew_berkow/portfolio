<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
	<title>Andrew Berkow</title>
        
	{!! HTML::style('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
        {!! HTML::style('bower_components/bootstrap-material-design/dist/css/ripples.min.css') !!}
        {!! HTML::style('bower_components/bootstrap-material-design/dist/css/material.min.css') !!}
        {!! HTML::style('css/font-awesome.css') !!}
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <style>
            body, h1, h2, h4, h5 h6, p, li, ul, li{
                
                font-family: 'Open Sans' sans serif;
            }
            
            
            .circle {
            border-radius: 50%;
            }
            
            .header_section{
                margin-top: 20px;
                margin-bottom: 20px;
                text-align: center;
            }
            
            .name{
                font-weight: 500;
                font-size: 22px;
                //float: right;
            }
            
            .header2{
                text-align: center;
            }
            
            .email{
                //float:right;
            }
            
            .email_fab{
                //float: right;
                margin-top: 10px;
            }
            
            .job-date{
                text-align: right;
            }
            .btn-act {
            margin: 0;
            padding: 0;
            font-size: 16px;
            width: 26px;
            height: 26px;
            border-radius: 100%;
            }
            
            h4{
                font-weight: 500;
font-size: 20px;
            }
            .readmore,
            .readless{
                text-align: center;
                margin-top: 15px;
                margin-bottom: 10px;
            }
            .joblist{
                margin-top: 15px;
            }
            .list-group .list-group-item .row-picture img, .list-group .list-group-item .row-action-primary img {
            background: white;
            padding: 1px;
            }
            
            
             
             li:before {    
                font-family: 'FontAwesome';
                content: '\f152';
                margin:0 5px 0 -15px;
                color: #00CC99;
                }
                ul {
  list-style-type: none;
  
  //padding-left: 20px;
}
.btn-linkin{
    background: #069!important;
    color: white!important;
}
.btn-facebook{
    background: #3a5795!important;
    color: white!important;
}

.btn-twitter{
    background: #66CCFF!important;
    color: white!important;
}
.list-group-item{
   color: #666666;
}
            
            
        </style>
        @yield('style')
        
        </head>
<body>
    
    <header id="masthead" class="site-header has-background" itemscope="itemscope" itemtype="" role="banner" style="background-color: #66CCFF; background-size: cover;">
        <div class="site-branding" style="
            height: 150px;">
            
        </div><!-- .site-branding -->
        <div class="shadow"></div>
    </header>
    
    
    <div class="container">
        <div class="well" style="display: block;
            margin-right: auto;
            margin-left: auto;
            margin-top: -100px;">
            <div class="row header_section">
                <div class="visible-xs">
                    <div class="pic ">
                        <img src="http://lorempixel.com/120/120" alt="" class="circle responsive-img">
                    </div>
                    <div class="">
                        <h2 class="name">Andrew Berkow</h2>
                        <p class="email">Andrewberkow45@gmail.com</p>
                        <div class="email_fab">
                            <button type="button" class="btn btn-primary btn-act btn-raised mdi-communication-email"  data-hover="tooltip" title="Email" data-placement="right"></button>
                            <button type="button" class="btn btn-linkin btn-act btn-raised fa fa-linkedin"  data-hover="tooltip" title="LinkedIn" data-placement="right"></button>
                            <button type="button" class="btn btn-facebook btn-act btn-raised fa fa fa-facebook"  data-hover="tooltip" title="LinkedIn" data-placement="right"></button>
                            <button type="button" class="btn btn-twitter btn-act btn-raised fa fa-twitter"  data-hover="tooltip" title="LinkedIn" data-placement="right"></button>
                        </div>
                    </div>
                </div>
                <div class="hidden-xs">
                    <div class="pic">
                        <img src="http://lorempixel.com/160/160" alt="" class="circle responsive-img">
                    </div>
                    <div class="header2">
                        <h1>Andrew Berkow</h1>
                        <h3>Andrewberkow45@gmail.com<h3>
                        <div class="">
                            <button type="button" class="btn btn-primary btn-fab btn-raised mdi-communication-email"  data-hover="tooltip" title="Email" data-placement="right"></button>
                            <button type="button" class="btn btn-linkin btn-fab btn-raised fa fa-linkedin"  data-hover="tooltip" title="Email" data-placement="right"></button>
                            <button type="button" class="btn btn-facebook btn-fab btn-raised fa fa fa-facebook"  data-hover="tooltip" title="Email" data-placement="right"></button>
                            <button type="button" class="btn btn-twitter btn-fab btn-raised fa fa-twitter"  data-hover="tooltip" title="Email" data-placement="right"></button>
                            <!--<button type="button" class="btn btn-success btn-fab btn-raised mdi-communication-email"  data-hover="tooltip" title="LinkedIn" data-placement="right"></button>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 skills">
                    <h3>Skills</h3>
                    <p>Java, PHP, Javascript, Jquery, AJAX, SQL, HTML 5, CSS 3, XML, Eloquent ORM, MVC, Laravel, 

                    Zend, version control (git and svn), RESTful, CLI, computer networks, LAMP, SSH, FTP, IBM SPSS, 

                    Android Studio, Corona SDK, LUA, CMS, Wordpress, Drupal, Magneto, OO programming, Ruby, Ruby 

                    on Rails, Microsoft Office Suite, Microsoft 360 Suite, LINUX, Mac OSX, Winsows, others.</p>
                </div>
                <div class="col-md-12 edu">
                    <h3>Education</h3>
                    <ul>
                        <li >Frostburg State University, B.S. Economics and a minor in Computer Science</li>
                        <li>2010-2014</li>
                        <li >GPA 3.1  Dean’s List Spring 2012 and Spring 2013</li>
                        <li>Attending Frostburg in Fall 2015 to start MS in Computer Science</li>
                        <li >Relevant coursework: Computer Science I (OO Java Programming), Computer Science II 
                            (Abstract Data Types and Algorithm Analysis), Computer Networks and Software Engineering 
                        </li>
                    </ul>
                    
                </div>
                <div class="col-md-12 work">
                    <h3>Employment</h3>
                  
                    <div class="list-group">
                        <div class="list-group-item">
                            <div class="row-action-primary">
                                <img src="http://www.openprofessionalgroup.com/sites/all/themes/opgv4/i/opg/opg_logo_short.png" alt="" class="circle responsive-img">
                            </div>
                            <div class="row-content">
                                <div class="least-content hidden-xs"><p>June 2012 – Present</div>
                                <h4 class="list-group-item-heading">OPG</h4>
                                <p class="list-group-item-text visible-xs">June 2012 – Present</p>
                                <p class="list-group-item-text"><span class="position">Junior Software Developer</span></p>
                                <p class="list-group-item-text"><a href="openprofessionalgrouop.com" target="_blank">openprofessionalgroup.com</a></p>
                                <div style="text-align: center;"><button id = "readmore_1" class="btn btn-default btn-act btn-raised readmore mdi-hardware-keyboard-arrow-down ab_action"
                                                                         action-show='#job1list'
                                                                         action-hide="#readmore_1"></button></div>
                                
                            </div>
                           
                            <ul  id="job1list" style="display:none;" class="joblist">
                            <li>Developed high quality, data driven, web applications with a team of web professionals. 

                            Responsibilities on this team included analysis, relational database architecture, coding, 

                            debugging, minimal project management and unit testing. Work was done according to 

                            specification documentation and mock ups. Platforms were built in PHP MVC and CMS.</li>
                            <li>Followed steps of  software development  life cycle; projects either followed an agile 

                                development process or a combination of agile and scrum. Adapted to new clients and 

                                technologies as required. Code developed was to be salable and reusable.</li>
                            <li>Created documentation for procedures to handle HPI according to HIPPA software compliance 

                            standards. Had software development security training that covered best practices for creating 

                            secure web software and combating major security threats and vulnerabilities.</li>
                            <li>Maintained strong communication with quality assurance department, graphic design, analysts, 

                            project managers, clients and telecommuting developers. Provided time estimates and hand offs.</li>
                            <div style="text-align: center;"><button class="btn btn-default btn-act btn-raised readless mdi-hardware-keyboard-arrow-up ab_action"
                                                                     action-show='#readmore_1'
                                                                     action-hide="#job1list"></button></div>
                        </ul>
                            
                            
                        </div>
                        <!--<div class="list-group-separator"></div>-->
                        <div class="list-group-item">
                            <div class="row-action-primary">
                                <img src="http://www.openprofessionalgroup.com/sites/all/themes/opgv4/i/opg/opg_logo_short.png" alt="" class="circle responsive-img">
                            </div>
                            <div class="row-content">
                                <div class="least-content hidden-xs"><p>Dec 2012 - Jan 2014</p></div>
                                <h4 class="list-group-item-heading">OPG</h4>
                                <p class="list-group-item-text visible-xs">Dec 2012 - Jan 2014</p>
                                <p class="list-group-item-text"><span class="position">Intern: Quality Assurance</span></p>
                                <p class="list-group-item-text"><a href="openprofessionalgrouop.com" target="_blank">openprofessionalgroup.com</a></p>
                                <div style="text-align: center;"><button id = "readmore_2" class="btn btn-default btn-act btn-raised readmore mdi-hardware-keyboard-arrow-down ab_action"
                                                                         action-show='#job2list'
                                                                         action-hide="#readmore_2"></button></div>
                                
                            </div>
                            <ul id = "job2list" style="display:none;" class="joblist">
                                    <li>Provided development support and quality assurance for web application listed in above position. 

                                        Participated in iterative rounds between testers and developers to fix all issues.</li>
                                    <li>Tested applications according to specification documents. Documented bugs, features that did not 

        meet specifications, errors in responsive layout and flaws in user experience.</li>
                                    <li>Worked on admin portals of CMS platforms to update media, pages, content and navigation.</li>
                                    <div style="text-align: center;"><button class="btn btn-default btn-act btn-raised readless mdi-hardware-keyboard-arrow-up ab_action"
                                                                     action-show='#readmore_2'
                                                                     action-hide="#job2list"></button></div>
                                </ul>
                            
                        </div>
                        <!--<div class="list-group-separator"></div>-->
                        <div class="list-group-item">
                            
                            <div class="row-action-primary">
                                <img src="img/fsu.png" alt="" class="circle responsive-img">
                            </div>
                            <div class="row-content">
                                <div class="least-content hidden-xs"><p>Aug 2011- Aug 2012</p></div>
                                <h4 class="list-group-item-heading">Frostburg State University Tutoring Center</h4>
                                <p class="list-group-item-text visible-xs">Aug 2011- Aug 2012</p>
                                <p class="list-group-item-text"><span class="position">Tutor</span></p>
                                <p class="list-group-item-text"><a href="http://www.frostburg.edu/admin/provost/academic-success-network/tutor/?forceAbsoluteURLs=false&skipGA=false" target="_blank">frostburg.com/tutor</a></p>
                                <div style="text-align: center;"><button id = "readmore_3" class="btn btn-default btn-act btn-raised readmore mdi-hardware-keyboard-arrow-down ab_action"
                                                                         action-show='#job3list'
                                                                         action-hide="#readmore_3"></button></div>
                                
                            </div>
                            <ul id = "job3list" style="display:none;" class="joblist">
                                    <li>Scheduled individual meetings with students and logged time details for each meeting.</li>
                            <li>Held weekly group tutoring sessions in a classroom. Up to eight students at a time would attend. 

                                Subjects Tutored were economics, computer science, history and communications.</li>
                            <div style="text-align: center;"><button class="btn btn-default btn-act btn-raised readless mdi-hardware-keyboard-arrow-up ab_action"
                                                                     action-show='#readmore_3'
                                                                     action-hide="#job3list"></button></div>
                        </ul>
                        </div>
                        <!--<div class="list-group-separator"></div>-->
                    </div>
                </div>
                                                                          
            </div>
        </div>
        
    
    </div>
	@yield('content')

	<!-- Scripts -->
       
	{!! HTML::script('bower_components/jquery/dist/jquery.min.js') !!}
        {!! HTML::script('bower_components/bootstrap/dist/js/bootstrap.min.js') !!}
        {!! HTML::script('bower_components/bootstrap-material-design/dist/js/ripples.min.js') !!}
        {!! HTML::script('bower_components/bootstrap-material-design/dist/js/material.min.js') !!}
        <script>
            $.material.init();
            
                
            $('body').on('click', '.ab_action', function(e){
                e.preventDefault();
                var element = $(this);
                var show = element.attr('action-show');
                var hide = element.attr('action-hide');
                
                $(show).show();
                $(hide).hide();
            });
            
            
            
            
            </script>
        @yield('script')
        
        </body>
</html>